<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PegawaiController extends Controller
{
    public function index()
    {
    	// mengambil data dari table pegawai
    	// $pegawai = DB::table('pegawai')->get();
    	$dd = DB::table('pegawai')->paginate(5);
 
    	// mengirim data pegawai ke view index
    	// return view('pegawai.index',['pegawai' => $pegawai]);
    	return view('pegawai.index',['pegawai' => $dd]);
    }
    public function tambah()
    {
    	// memanggil view tambah
    	return view('pegawai.tambah');
    }
    public function store(Request $request)
    {
	// insert data ke table pegawai
    $this->validate($request,[
        'nama' => 'required|min:5|max:20',
        'jabatan' => 'required',
        'umur' => 'required|numeric|max:100',
    ]);
	DB::table('pegawai')->insert([
		'pegawai_nama' => $request->nama,
		'pegawai_jabatan' => $request->jabatan,
		'pegawai_umur' => $request->umur,
		'pegawai_alamat' => $request->alamat
	]);
	// alihkan halaman ke halaman pegawai
	return redirect('/pegawai');
}
    // method untuk edit data pegawai
    public function edit($id)
    {
        // mengambil data pegawai berdasarkan id yang dipilih
        $pegawai = DB::table('pegawai')->where('pegawai_id',$id)->get();
        // passing data pegawai yang didapat ke view edit.blade.php
        return view('pegawai.edit',['pegawai' => $pegawai]);
    
    }

    public function update(Request $request)
    {
        // update data pegawai
        DB::table('pegawai')->where('pegawai_id',$request->id)->update([
            'pegawai_nama' => $request->nama,
            'pegawai_jabatan' => $request->jabatan,
            'pegawai_umur' => $request->umur,
            'pegawai_alamat' => $request->alamat
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/pegawai');
    }

    public function hapus($id)
    {
        // hapus data pegawai
        DB::table('pegawai')->where('pegawai_id',$id)->delete();
        // alihkan halaman ke halaman pegawai
        return redirect('/pegawai');
    }
}