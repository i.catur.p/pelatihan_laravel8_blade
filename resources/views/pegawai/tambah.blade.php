<!DOCTYPE html>
<html>
<head>
   <title>Pelatihan Membuat CRUD Pada Laravel<</title>
</head>
<body>
   <h3>Data Pegawai</h3>
   <a href="/pegawai"> Kembali</a>

   <!-- Menampilkan error validasi -->
  @if (count($errors) > 0)
  <div class="alert alert-danger">
   <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
   </ul>
  </div>
  @endif
  <!-- Akhir Menampilkan error validasi -->
   <br/>
   <br/>
   <form action="/pegawai/store" method="post">
       {{ csrf_field() }}
       <label for="nama">Nama:</label><br>
       <input type="text" id="nama" name="nama"><br>
       <label for="jabatan">Jabatan:</label><br>
       <input type="text" id="jabatan" name="jabatan"><br>
       <label for="umur">Umur:</label><br>
       <input type="number" id="umur" name="umur"><br>
       <label for="alamat">Alamat:</label><br>
       <input type="text" id="alamat" name="alamat"><br>
       <input type="submit" value="Simpan Data">
   </form>
</body>
</html>