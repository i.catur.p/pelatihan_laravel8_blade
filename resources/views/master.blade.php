<!DOCTYPE html>
<html>
<head>
	<title>Belajar Laravel Blade Template</title>
</head>
<body>
 
	<header>
 
		<h2>Blog</h2>
		<nav>
			<a href="/blog">HOME</a>
			|
			<a href="/blog/tentang">TENTANG</a>
			|
			<a href="/blog/kontak">KONTAK</a>
		</nav>
	</header>
	<hr/>
	<br/>
	<br/>
 
	<!-- bagian judul halaman blog -->
	<h3> @yield('judul_halaman') </h3>
 
 
	<!-- bagian konten blog -->
	@yield('konten')
 
 
	<br/>
	<br/>
	<hr/>
	<footer>
		<p>Ini Footer</p>
	</footer>
 
</body>
</html>